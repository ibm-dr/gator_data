"""
Each dialog entry is a dict containing a list of utterances keyed by the ID of the dialog. 

Each utterance is a dict with two keys, which are lists of strings: history and candidates. 

history: history: [dialog_turn_0, ... dialog_turn N], where N is an even number since the agent starts every conversation.
candidates: [next_utterance_candidate_1, ..., next_utterance_candidate_19]
        The last candidate is the ground truth response observed in the conversational data

TODO:
    [] - Spaces before periods at end of sentences
    [] - everything lowercase
"""
EXAMPLE_ENTRY = {
    "20180904-045349_715_live": [
        {
          "history": [],
          "candidates": [
            "I can donate .50. I'm a caregiver for a child with different abilities so I hope that helps.",
            "well All for a good Course i will give and never regret it.....if donations are giving for the  helped  needed any where in the world i do not really mind how about you...?",
            "Yes that seems like something I would love to do",
            "I'm well, thanks for asking. ",
            "That was a quick unthoughtful answer. No consideration for children in need Huh?",
            "Okay,  so we are in agreement on the payment  to the charity. I assume save the children would be able helping children in three world countries. I wish they were way more specific in the instructions on both ends.",
            "That's understandable. We all do what we can to help each other. I donate to Save the Children. Have you heard about them?",
            "Yeah been off for a couple of days. It's nice.",
            "I am only receiving so much from this survey. So as much as I would love to I would not like to donate at this time.",
            "Same here.  It makes me angry/sad that there are so many who are going hungry and I can't help feed more.",
            "Good morning. How are you doing today?"
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?"
          ],
          "candidates": [
            "Ok, that is great, I just wanted to give you a bit more information about what Save the Children is fighting against.... For instance, millions of Syrian children have grown up facing the daily threat of violence. In the first two months of 2018 alone, 1,000 children were reportedly killed or injured in intensifying violence. ",
            "They seem like they do a good work",
            "Many of them lives on a war zone",
            "Yes, possibly. There is a lack of support for children in developing countries, especially in war zones.",
            "I think, biut not sure. Tell me more.",
            "There is a wonderful charity called Save the Children. They help children all over the world including right here America",
            "North Carolina, I've been to Florida once, it's nice.",
            "I'm doing well.  How are you?",
            "Let me ask, are you also able to donate, and if so how much are you able to spare?",
            "Oh that's awesome!! What kind of programs do they provide?",
            "I'm doing pretty good for a Tuesday morning. "
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday."
          ],
          "candidates": [
            "ya humidity is very high",
            "So do I. Do you regularly donate your time or money to Save the Children?",
            "Great question. No, we work all around the world, wherever our help is needed.",
            "Interesting answer, I think it's a mixture the world over",
            "I think you are failing at achieving their goal here. ",
            "I am good do you have any children?",
            "Okay, well do you donate much?",
            "What type of clip was it? Were the children without family and the necessary tools to survive?",
            "At this time I can't donate any amount. However I will donate later.",
            "What is the mission of Save the Children?",
            "Ugh yes it does!"
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday.",
            "Ugh yes it does!",
            "I can not believe how warm it is already."
          ],
          "candidates": [
            "They help out many third world countries from all over the world.",
            "At the end of the HIT it'll give you a box to input any amount you'd like to donate to the children.",
            "You too! Thanks!",
            "That's not enough for me honestly. A picture and a profile isn't enough for me to think about giving away some of my reward.",
            "ok great thanks! have a nice weekend. i'll be praying for your health to improve.",
            "I don't know a lot about it, but from what I do know, they seem like a generous organization. ",
            "I understand that. Believe me I do. How about 50 cents?",
            "Well, your donation would help on so many levels.  Your donation will help to the overall betterment, improved health, education, and safety of many of these children who come from a developing country.",
            "I just read that they actually helped over 237k children the US too! I'm glad they're helping people here too.",
            "Alrigh peace be with you",
            "Where are you from? "
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday.",
            "Ugh yes it does!",
            "I can not believe how warm it is already.",
            "Where are you from? ",
            "I am from the Midwest. What about you?"
          ],
          "candidates": [
            "Yes it does provide vaccines that help them live longer.  You do want the helpless children to have a chance?",
            "I am great and you?",
            "At this time I am not willing to do that",
            "In the past few years I rarely have.",
            "Good.  How are you doing?",
            "no children in need all over the world. It's US based website please see",
            "I would love to. Save the Children helps to ensure children's rights to health, education, and safety. Is this something that you care about?",
            "trains teachers to engage their students through more effective teaching practices. coaches parents and caregivers to help their children learn early on, so they are prepared to enter school. offers ways for parents and community volunteers to get kids reading and doing math outside of school hours. introduces children to the power of artistic expression \u00a1\u00aa drawing, painting, music, drama, dance and more \u00a1\u00aa to help them heal, learn and do better in school.",
            "Do you do any volunteering?",
            "Is the organization non-profit?",
            "I'm from the South East. It's always warm here. "
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday.",
            "Ugh yes it does!",
            "I can not believe how warm it is already.",
            "Where are you from? ",
            "I am from the Midwest. What about you?",
            "I'm from the South East. It's always warm here. ",
            "Oh, yep. You are definitely in for warm weather, which is great as far as I am concerned."
          ],
          "candidates": [
            "I'm doing good. I was wondering if I could talk to you about donating to Save the Children today? ",
            "Yes they will",
            "All the amount donated by you is going for the charity",
            "I am not going to waste the reward.  I use it for things that my family needs.",
            "That's so awesome that you want to help out! You can choose to donate any amount of the task payment, $0-$2 or anything in between. How much would you like to donate to the charity now?",
            "If it is coming from my payment today it is something that I don't have yet...so maybe I can do 0.20.",
            "I think $2 would be the best way to go.",
            "I'm doing really well.  I was wondering how familiar you are with the effect all the intensifying violence in the world is having on children?",
            "Tha k you so much for the infkrmation",
            "okay.... i think this is about children's charity",
            "We're about to get hit by a tropical storm."
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday.",
            "Ugh yes it does!",
            "I can not believe how warm it is already.",
            "Where are you from? ",
            "I am from the Midwest. What about you?",
            "I'm from the South East. It's always warm here. ",
            "Oh, yep. You are definitely in for warm weather, which is great as far as I am concerned.",
            "We're about to get hit by a tropical storm.",
            "I heard that some bad weather was going to be coming. I hope it is not too severe."
          ],
          "candidates": [
            "That would be amazing and so appreciated!  You have such a good heart!",
            "I like to help children in the Unites States - we have so many children here with cancer and other diseases.",
            "They provide things such as food and medical treatment. Things that are basic needs are given to them. ",
            "I will stay with my previous response, I will donate $0.",
            "Hello, what do the Save the Children Federation do?",
            "I donate sometimes well, to other charities",
            "I do. I give to a few charities.",
            "I believe I have yes.",
            "I remember do that as well as a kid. I always felt great. I hope my small donation gives another child job as well",
            "The organization I am asking for a donation today is Save The Children. Do you want to know more?",
            "Me too. It's just part of living on the Gulf. You have to be prepared for it."
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday.",
            "Ugh yes it does!",
            "I can not believe how warm it is already.",
            "Where are you from? ",
            "I am from the Midwest. What about you?",
            "I'm from the South East. It's always warm here. ",
            "Oh, yep. You are definitely in for warm weather, which is great as far as I am concerned.",
            "We're about to get hit by a tropical storm.",
            "I heard that some bad weather was going to be coming. I hope it is not too severe.",
            "Me too. It's just part of living on the Gulf. You have to be prepared for it.",
            "Yes, I am sure you get a lot of storms."
          ],
          "candidates": [
            "Exactly! Well, it was nice to meet you and thank you for agreeing to donate today!",
            "They are a group dedicated to helping children in danger and need around the world.  They help everywhere from the Rohingya refugees to children devastated by Huricane Harvey down in your region of the US",
            "Ok so what about save the children?",
            "it is so hard to make a choice",
            "That is very interesting, Are they clear with their financial reports? I mean is it a trustworthy organization?",
            "Of coarse, and I wish more people could see it that way.",
            "Good. It's a very strong and important organization. The lack of child support in developing countries, such as in Syria, has been growing for years now.",
            "how long has STC been around?",
            "i honestly prefer St Jude. If it wasnt for donations being made, my cousin might not be here today.  He goes back every year now to get a check up and will for life at no cost.  ",
            "Do you mind me asking how much you made your first month.",
            "We do. I guess I should get into what this chat is supposed to be about. Have you heard of the Charity Save The Children?"
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday.",
            "Ugh yes it does!",
            "I can not believe how warm it is already.",
            "Where are you from? ",
            "I am from the Midwest. What about you?",
            "I'm from the South East. It's always warm here. ",
            "Oh, yep. You are definitely in for warm weather, which is great as far as I am concerned.",
            "We're about to get hit by a tropical storm.",
            "I heard that some bad weather was going to be coming. I hope it is not too severe.",
            "Me too. It's just part of living on the Gulf. You have to be prepared for it.",
            "Yes, I am sure you get a lot of storms.",
            "We do. I guess I should get into what this chat is supposed to be about. Have you heard of the Charity Save The Children?",
            "I have heard about them. What do you like about them?"
          ],
          "candidates": [
            "It is and a very reputable one. Have you ever donated to one before?",
            "We do, but it is hard to qualify because of the difficult conditions. We only accept the best. ",
            "86 percent of the donation actually goes to helping the children. 9 percent gets reinvested into marketing for the charity at 5 percent into miscellaneous expenses.  As you can see much of the donation will go to helping these poor children. ",
            "Yes hello, how are you?",
            "This is a great organization and I am sure that they would not be allowed to continue the way they do if the money was not going where it is suppose to and every penny being accounted for.  They do so much good in the world yes there are hungry kids here but millions of Syrian children have grown up facing the daily threat of violence. In the first two months of 2018 alone, 1,000 children were reportedly killed or injured in intensifying violence.",
            "Sounds like they do some good things.  Kids are important.",
            "I'd heard something about that. Many are suffering there. ",
            "I'm going to keep the amount I'm going to donate to myself.  I feel awkward discussing that with a stranger.",
            "That is completely understood too. And the researchers have made it very clear that all proceeds obtained are sent directly to the charity and to the childrens' needs.",
            "I'm glad you are safe!",
            "I like that they're committed to helping children in need. They're very transparent in their work and do great things to help children in underprivileged countries. "
          ]
        },
        {
          "history": [
            "Good morning. How are you doing today?",
            "Hi. I am doing good. How about you?",
            "I'm doing pretty good for a Tuesday morning. ",
            "Haha. Same here, but it really feels like a Monday.",
            "Ugh yes it does!",
            "I can not believe how warm it is already.",
            "Where are you from? ",
            "I am from the Midwest. What about you?",
            "I'm from the South East. It's always warm here. ",
            "Oh, yep. You are definitely in for warm weather, which is great as far as I am concerned.",
            "We're about to get hit by a tropical storm.",
            "I heard that some bad weather was going to be coming. I hope it is not too severe.",
            "Me too. It's just part of living on the Gulf. You have to be prepared for it.",
            "Yes, I am sure you get a lot of storms.",
            "We do. I guess I should get into what this chat is supposed to be about. Have you heard of the Charity Save The Children?",
            "I have heard about them. What do you like about them?",
            "I like that they're committed to helping children in need. They're very transparent in their work and do great things to help children in underprivileged countries. ",
            "Yes, I also like what they do. They are a great organization."
          ],
          "candidates": [
            "I'm well, you?",
            "How many people are currently donating?",
            "Just found out there are tons of sick kids right now over seas that need help",
            "I want to be able to start my own charity organizations to help children, animals, cancer patients, and a lot more. I will pray for your heart to not change because you seem to be very kind hearted!",
            "I think I'm interested but how would I donate from here?",
            "Yeah... I notice that \"The research team will collect all donations and send it to Save the Children.\" but I think it\\'s a world wide thing..right?",
            "oh that's cool.   I've always wanted to do something like that, instead of working in the field I do now.",
            "Yes, I do.",
            "I guess I'm supposed to persuade you to donate your bonus to the save the children charity.....I want you to keep your bonus obviously",
            "Ok thanks you have a nice day...",
            "I'm planning on donating most of my earnings today. Would you like to donate as well?"
          ]
        }
  ]
}
