## Datasets for the GATOR IBM Dialog Research Project

Raw data source files are available under `data/src/` and the processed datasets are in `data/persuasion_for_good`. 


### Data Format
see `example_entry.py`, and the comment at the top.


### File Structure

```
└── gator_data
    ├── data
    │   ├── persuasion_for_good
    │   │   ├── persuadee_dataset.json
    │   │   └── persuader_dataset.json
    │   └── src
    │       └── PersuasionForGood_dialogs.csv
    ├── .gitignore
    ├── LICENSE
    └── README.md

```